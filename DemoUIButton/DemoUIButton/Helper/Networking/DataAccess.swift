//
//  DataAccess.swift
//  KOSIGN_LMS
//
//  Created by lymanny on 16/3/22.
//

import Foundation
import UIKit

class DataAccess {
    
    public static var sharedInstance    = DataAccess()
    
    // The Network access for request, response, upload and download task
    private static var sessionConfig    : URLSessionConfiguration!
    private static var session          : URLSession!
    
    static var shared: DataAccess = {
        // Timeout Configuration
        sessionConfig                               = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest     = 120.0
        sessionConfig.timeoutIntervalForResource    = 120.0
        session = URLSession(configuration: sessionConfig)
        return sharedInstance
    }()
    
    private func manualError(err: NSError) -> NSError {
        // Custom NSError
        #if DEBUG
        print("Connection server error : \(err.domain)")
        #endif
        switch err.code  {
            
            /**  -1001 : request timed out
             -1003 : hostname could not be found
             -1004 : Can't connect to host
             -1005 : Network connection lost
             -1009 : No internet connection
             */
        case -1001, -1003, -1004: // request timed out
            let error = NSError(domain: "NSURLErrorDomain", code: err.code, userInfo: [NSLocalizedDescriptionKey : "connection_time_out".localized])
            return error
        case -1005 : // Network connection lost
            let error = NSError(domain: "NSURLErrorDomain", code: err.code, userInfo: [NSLocalizedDescriptionKey : "internet_connection_is_unstable_please_try_again_after_connecting".localized])
            return error
        case -1009 : // No internet connection
            let error = NSError(domain: "NSURLErrorDomain", code: err.code, userInfo: [NSLocalizedDescriptionKey : "internet_connection_is_unstable_please_try_again_after_connecting".localized])
            return error
        default :
            return err
        }
    }
    
    private init() {}
    
    private func showHideLoading(isShow: Bool) {
        DispatchQueue.main.async {
            if isShow {
                LoadingView.show()
            }
            else {
                LoadingView.hide()
            }
        }
    }
    
    // Request data task with API and response data & error as completion
    func fetch<I: Encodable, O: Decodable>(shouldShowLoading  : Bool = true,
                                           apiKey             : APIKey,
                                           urlStr             : String = "",
                                           httpMethod         : HTTPMethod = .POST,
                                           body               : I,
                                           responseType       : O.Type,
                                           completion         : @escaping (Result<O?, NSError>) -> Void) {
        
        if shouldShowLoading {
            DispatchQueue.main.async {
                LoadingView.show()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        }
        
        
        let request = self.getURLRequest(apiKey     : apiKey,
                                         urlStr     : urlStr,
                                         body       : body,
                                         httpMethod : httpMethod)
        
        DataAccess.session.dataTask(with: request) { (data, response, error) in
            
            //ShowLoadingimagView
            if shouldShowLoading {
                DispatchQueue.main.async {
                    LoadingView.hide()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
            
            //MARK: - Check Data, Response Error
            guard
                let url = response?.url,
                let httpResponse = response as? HTTPURLResponse,
                let fields = httpResponse.allHeaderFields as? [String: String]
            else {
               
                if let nsError = error as NSError? {
                    #if DEBUG
                    print("""
                    \(nsError.code) | \(nsError.localizedDescription)
                    """)
                    #endif
                   
                } else {
                    let nsError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "error_occurred_during_process".localized])
                    #if DEBUG
                    print("""
                    \(nsError.code) | \(nsError.localizedDescription)
                    """)
                    
                    self.manualError(err: (error as? NSError)!)
                    
                    #endif
                }
                return
            }
            
            guard let data = data else {
                
                let nsError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "error_occurred_during_process".localized])
                #if DEBUG
                print("""
                \(nsError.code) | \(nsError.localizedDescription)
                """)
                #endif
                
                return
            }
            
            let decodedDataString  = String(data: data, encoding: String.Encoding.utf8)?.removingPercentEncoding
            
            
            guard let responseData = decodedDataString == nil ? data : decodedDataString?.data(using: .utf8) else {
                let nsError = NSError(domain: "ClientError", code: 168, userInfo: [NSLocalizedDescriptionKey : "Could not convert string to data."])
                
                #if DEBUG
                print("""
                \(nsError.code) | \(nsError.localizedDescription)
                """)
                #endif
                return
            }
            
            #if DEBUG
            Log("""
                \(request.url!) | \(apiKey.rawValue)
                \(responseData.prettyPrinted)
                """)
            #endif
            
            
            let responseDataDic = responseData.dataToDic
        
            guard let responseObject = try? JSONDecoder().decode(responseType, from: responseData) else {
                
                if responseData.prettyPrinted == "{\n\n}" {
                    print("Pretty Print nnnn")
                } else {
                    print("Response has no key error")
                }
                return
            }
            
            // Set cookie to use with Web
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: fields, for: url)
            HTTPCookieStorage.shared.setCookies(cookies, for: url, mainDocumentURL: nil)
            for cookie in cookies {
                var cookieProperties        = [HTTPCookiePropertyKey: Any]()
                cookieProperties[.name]     = cookie.name
                cookieProperties[.value]    = cookie.value
                cookieProperties[.domain]   = cookie.domain
                cookieProperties[.path]     = cookie.path
                cookieProperties[.version]  = cookie.version
                cookieProperties[.expires]  = Date().addingTimeInterval(31536000)
                
                Shared.share.jSessionId     = "\(cookie.name)=\(cookie.value)"
                
                if let cookie = HTTPCookie(properties: cookieProperties) {
                    HTTPCookieStorage.shared.setCookie(cookie)
                }
                
                #if DEBUG
                print("name: \(cookie.name) value: \(cookie.value)")
                #endif
            }
            
            //MARK: - Check ERROR:
            if let dic = responseDataDic["error"] {
                let error       = NSError(domain: "ClientError", code: 168, userInfo: [NSLocalizedDescriptionKey : dic])
                let errorDic    = dic as? NSDictionary
                
                if let errorCode = errorDic?["code"] as? String {
                    if errorCode == "ERP_REQUEST_TIMEOUT" {
                        DispatchQueue.main.async {
                            completion(.failure(error))
                        }
                    }
                    // Token and Session Expired
                    else if errorCode == "SESSION_EXPIRED" {
                        DispatchQueue.main.async {
                            completion(.failure(error))
                        }
                        
                    } else if errorCode == "ERP_SERVICE_ERROR" || errorCode == "BAD_REQUEST" {
                        DispatchQueue.main.async {
                            completion(.failure(error))
                        }
                    }
                    
                    else {
                        DispatchQueue.main.async {
                            completion(.failure(error))
                            }
                        }
                    }
                } else {
                if let codeError = responseDataDic["code"] as? String {
                    let error    = NSError(domain: "ClientError", code: 168, userInfo: [NSLocalizedDescriptionKey : codeError])
                    if codeError == "UNAUTHORIZED" {
                        DispatchQueue.main.async {
                            completion(.failure(error))
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        completion(.success(responseObject))
                    }
                }
            }
            
        }.resume()
        
    }
    
    //MARK: - GET REQUEST URL -
    private func getURLRequest<T: Encodable>(apiKey: APIKey,
                                             urlStr: String = "",
                                             body: T,
                                             httpMethod : HTTPMethod = .POST) -> URLRequest {
        
        func queryString<T:Encodable>(body:T) -> String {
            let request     = body
            guard let str   = request.asJSONString() else { return "" }
            return str
        }
        
        var url : URL!
        
        url = URL(string: "\(apiKey.rawValue)")
        
        
        var request         = URLRequest(url: url!)
        request.httpMethod  = httpMethod.rawValue
        
        let strQuery = queryString(body: body)
        
        if httpMethod != .GET {
            request.httpBody = strQuery.data(using: .utf8)
        }
        
        guard let cookies = HTTPCookieStorage.shared.cookies(for: request.url!) else {
            return request
        }
        
        
        request.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookies)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.addValue("\(XAppVersion.base.rawValue)", forHTTPHeaderField: "X-App-Version")
        request.addValue("\(Shared.language.rawValue)", forHTTPHeaderField: "Accept-Language")
        
//         set Authorization = "" : using with api logout when token is empty
//         Set Authorization = "" when request code list. because code list doesn't need Authorization
//        if Shared.share.isRequestWithoutAuthorize == false {
//            request.addValue((Shared.share.loginData?.token == nil) ? "" : "Bearer \("\(Shared.share.loginData?.token ?? "")")", forHTTPHeaderField: "Authorization")
//        } else {
//            request.addValue("", forHTTPHeaderField: "Authorization")
//            Shared.share.isRequestWithoutAuthorize = false
//        }
        
        Log.r("""
            Request: \(request)
            Header : \(String(describing: request.allHTTPHeaderFields))
            Method : \(String(describing: request.httpMethod))
            BODY   : \(strQuery)
            """)
        
        return request
    }
    
}

