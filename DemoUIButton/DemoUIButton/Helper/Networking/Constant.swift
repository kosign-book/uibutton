//
//  Constant.swift
//  KOSIGN_LMS
//
//  Created by lymanny on 16/3/22.
//

import Foundation

typealias Completion                = ()                -> Void

enum HTTPMethod : String {
    case GET    = "GET"
    case POST   = "POST"
    case PUT    = "PUT"
    case PATCH  = "PATCH"
    case DELETE = "DELETE"
}



enum XAppVersion : String {
   case base = "20210705"

}
enum UserDefaultKey : String {
    case appLang            = "appLang"
}

enum NotifyKey : String {
    case reloadLocalize                     = "reloadLocalize"
    case pushToMeetingInfo                  = "pushToMeetingInfo"
    case pushMoreVC                         = "pushMoreVC"
}
enum APIKey: String {
    
    static var baseURL : String = "https://api.publicapis.org/"
    
    #if DEBUG
    case mg                         = "/api/v2/app/setting/9178a6ee-e434-4969-afcf-f161788348c4?os="
    #else
    case mg                         = "/api/v2/app/setting/9178a6ee-e434-4969-afcf-f161788348c4?os="
    #endif
    
    
}

