//
//  ButtonGradient.swift
//  KOSIGN_LMS
//
//  Created by Phea yuth on 5/2/22.
//

import Foundation
import UIKit


extension UIColor {
    func applyGradient(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(red: 0.02, green: 0.67, blue: 0.83, alpha: 1.00).cgColor, UIColor(red: 0.06, green: 0.46, blue: 0.67, alpha: 1.00).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
    }
}
