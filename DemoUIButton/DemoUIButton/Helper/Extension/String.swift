//
//  String.swift
//  KOSIGN_LMS
//
//  Created by lymanny on 16/3/22.
//

import Foundation
import UIKit

extension String {
    
    var localized: String {
        get {
            if let path     = Bundle.main.path(forResource: "LocalizeString", ofType: "json") {
                if let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) {
                    if let dic = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        // to get value of key
                        if let keyDic =  dic[self] as?  [String:String] {
                            let value = keyDic[Shared.language.rawValue]!
                            return value
                        }
                    }
                }
            }
            return self
        }
    }
    
    //MARK: - Function
    func replace(of: String, with: String) -> String {
        return self.replacingOccurrences(of: of, with: with)
    }
}
