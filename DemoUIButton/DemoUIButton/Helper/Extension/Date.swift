//
//  Date.swift
//  KOSIGN_LMS
//
//  Created by Sam Sophanna on 5/12/22.
//

import Foundation


extension Date {
    
    func dateFormat(format: String = "dd/MM/yyyy") -> String {
            let formatter = DateFormatter()
            formatter.timeZone = Calendar.current.timeZone
            formatter.dateFormat = format
            return formatter.string(from: self)
        }
    func timeFormat(format: String = "hh:mm a") -> String {
            let formatter = DateFormatter()
            formatter.timeZone = Calendar.current.timeZone
            formatter.dateFormat = format
            return formatter.string(from: self)
        }
    func dateTimeFormat(format: String = "d MMM hh:mm a") -> String {
            let formatter = DateFormatter()
            formatter.timeZone = Calendar.current.timeZone
            formatter.dateFormat = format
            return formatter.string(from: self)
        }
}
