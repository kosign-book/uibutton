//
//  LoginViewController.swift
//  DemoUIButton
//
//  Created by joseph on 4/10/22.
//

import UIKit

class LoginViewController: UIViewController {
    
    //Create senceManager is responsible for presenting scenes, requesting future scenes be downloaded, and loading assets in the background.
    var senceManager = (UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //When click on this button navigation to other UIViewController
    @IBAction func gotoStoreButton(_ sender: Any) {
        let homeVC = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "MainTapVC")
        self.senceManager.changeRootView(homeVC)
    }
}
