//
//  MenuViewModel.swift
//  DemoUIButton
//
//  Created by joseph on 3/10/22.
//

import Foundation
import UIKit

class MenuViewModel {
    var arrayMenu = [MenuModel]()
    
    func arrayData(){
        arrayMenu = [
            MenuModel(productName: "AirJordan3 Retro",      productImage: "AirJordan3Retro"     , likeNumber: ""),
            MenuModel(productName: "Jordan Point Lane",     productImage: "JordanPointLane"     , likeNumber: ""),
            MenuModel(productName: "JumpMan Two Trey",      productImage: "JumpmanTwoTrey"      , likeNumber: ""),
            MenuModel(productName: "NikeAir Force Brow",    productImage: "NikeAirForceBrown"   , likeNumber: ""),
            MenuModel(productName: "NikeDunk HighRetro",    productImage: "NikeDunkHighRetro"   , likeNumber: ""),
            MenuModel(productName: "ZionII",                productImage: "Zion2"               , likeNumber: ""),
            MenuModel(productName: "JordanPlay",            productImage: "JordanPlay"          , likeNumber: ""),
            MenuModel(productName: "AirJordan3 Retro",      productImage: "AirJordan3Retro"     , likeNumber: ""),
            MenuModel(productName: "Jordan Point Lane",     productImage: "JordanPointLane"     , likeNumber: ""),
            MenuModel(productName: "JumpMan Two Trey",      productImage: "JumpmanTwoTrey"      , likeNumber: ""),
            MenuModel(productName: "NikeAir Force Brow",    productImage: "NikeAirForceBrown"   , likeNumber: ""),
            MenuModel(productName: "NikeDunk HighRetro",    productImage: "NikeDunkHighRetro"   , likeNumber: ""),
            MenuModel(productName: "ZionII",                productImage: "Zion2"               , likeNumber: ""),
            MenuModel(productName: "JordanPlay",            productImage: "JordanPlay"          , likeNumber: ""),
           
        ]
    }
}
