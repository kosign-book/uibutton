//
//  MenuModel.swift
//  DemoUIButton
//
//  Created by joseph on 3/10/22.
//

import Foundation
import UIKit

struct MenuModel {
    
    let productName     : String?
    let productImage    : String?
    let likeNumber      : String?
}
