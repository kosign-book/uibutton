//
//  HomeVC.swift
//  DemoUIButton
//
//  Created by joseph on 3/10/22.
//

import UIKit

class HomeVC: UIViewController {
    
    @IBOutlet weak var tableView        : UITableView!
    
    let menuViewModel = MenuViewModel()
    var selected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuViewModel.arrayData()
        
    }

    @IBAction func sortRemoveRowAction(_ sender: Any) {
        tableView.isEditing = !tableView.isEditing
    }
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuViewModel.arrayMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        //        cell.nameProLabel.text = "Nike Air Force II"
        cell.config(data: menuViewModel.arrayMenu[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    //When selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        if cell.isSelected == true {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "ViewDetailSB", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewDetailVC") as! ViewDetailVC
            vc.title_name = menuViewModel.arrayMenu[indexPath.row].productName ?? ""
            vc.images     = UIImage(named: menuViewModel.arrayMenu[indexPath.row].productImage ?? "")!
            
            self.navigationController?.pushViewController(vc, animated: true)
           
        }
        
    }
    
    //allow to MoveRow
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //MoveRow
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = menuViewModel.arrayMenu[sourceIndexPath.row]
        menuViewModel.arrayMenu.remove(at: sourceIndexPath.row)
        menuViewModel.arrayMenu.insert(item, at: destinationIndexPath.row)
    }
    
}


