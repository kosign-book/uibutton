//
//  ViewDetailVC.swift
//  DemoUIButton
//
//  Created by Phanna on 9/10/22.
//

import UIKit

class ViewDetailVC: UIViewController, UIViewControllerTransitioningDelegate {
    
    //    var data   = MenuViewModel()
    
    @IBOutlet weak var buyButton    : UIButton!
    @IBOutlet weak var likeButton   : UIButton!
    
    @IBOutlet weak var nameLabel    : UILabel!
    @IBOutlet weak var imageView    : UIImageView!
    
    
    var images          = UIImage()
    var title_name      = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text  = title_name
        imageView.image = images
        CustomButton()
    }
    
    // This function for customize or set attrebutes ButtonLike and ButtonOrder
    func CustomButton(){
        
        //ButtonOrder
        buyButton.layer.shadowOpacity          = 0.3
        buyButton.layer.shadowOffset.width     = 1
        buyButton.layer.shadowOffset.height    = 1
        buyButton.layer.cornerRadius           = 7
        buyButton.backgroundColor              = UIColor(hexString: "BC3E31")
        buyButton.setTitleColor(.white,         for: .normal)
        buyButton.setTitle(" Order",            for: .normal)
        buyButton.setImage(UIImage(systemName: "cube.box.fill"), for: .normal)
        
        //ButtonLike
        likeButton.layer.shadowOpacity          = 0.3
        likeButton.layer.shadowOffset.width     = 1
        likeButton.layer.shadowOffset.height    = 1
        likeButton.layer.cornerRadius           = 7
        likeButton.backgroundColor              = UIColor(hexString: "4474B5")
        likeButton.setTitleColor(.white,         for: .normal)
        likeButton.setTitle(" Like ",            for: .normal)
        likeButton.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
    }
    
    
    @IBAction func buttonLikeAction(_ sender: UIButton) {
        sender.shake {
            print("Like Product")
        }
    }
    
    @IBAction func buttonOrderAction(_ sender: UIButton) {
        sender.press {
            print("Order")
            let storyboard = UIStoryboard(name: "AlertPopUpSB", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AlertPopUpVC")
            vc.modalPresentationStyle = .custom
            vc.transitioningDelegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
}
